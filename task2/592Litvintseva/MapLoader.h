//
// Created by darya on 4/16/19.
//

#ifndef OPENGL_TASKS2019_MAPLOADER_H
#define OPENGL_TASKS2019_MAPLOADER_H

#include <vector>
#include <algorithm>
#include <SOIL2.h>
#include <iostream>


class MapLoader {
public:
    std::vector<std::vector<float>> height_map;
    int width;
    int height;
    int channels;
    int max;
    double min;


    MapLoader()
            :max(0), min(1)
    {}

    void ReadMap() {
        unsigned char* ht_map = SOIL_load_image
                (
                        "592LitvintsevaData1/australia.png",
                        &width, &height, &channels,
                        SOIL_LOAD_L
                );

        if (ht_map == NULL) {
            std::cout << "NULL map, check file paths" << std::endl;
            std::__throw_runtime_error("error");
        }

        assert(channels == 1);

        for (int i = 0; i < height; ++i) {
            height_map.push_back(std::vector<float>(width));
            for (int j = 0; j < width; ++j) {
                height_map[i][j] = static_cast<float>(ht_map[j]);

                if (max < height_map[i][j]) {
                    max = height_map[i][j];
                }
            }
            ht_map += width;
        }

        normalize();
    }

private:
    void normalize() {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                height_map[i][j] /= max;
                if (height_map[i][j] < min) {
                    min = height_map[i][j];
                }
            }
        }
    }
};

#endif //OPENGL_TASKS2019_MAPLOADER_H
