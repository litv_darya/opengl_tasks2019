/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D sandTex;
uniform sampler2D grassTex;
uniform sampler2D stoneTex;
uniform sampler2D snowTex;

in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in float h;
in vec3 color_tmp;

out vec4 fragColor; //выходной цвет фрагмента

const float sigma = 0.7;

float norm_coef(float x, float mu, float sigma_) {
    return exp(-((x - mu) * (x - mu) / (2 * sigma_*sigma_)));
}

void main()
{
    vec3 diffuseColor = texture(sandTex, texCoord).rgb * norm_coef(h, 0, sigma/7) +
                        texture(grassTex, texCoord).rgb * norm_coef(h, 2*sigma/7, sigma/7) +
                        texture(stoneTex, texCoord).rgb * norm_coef(h, 4*sigma/7, sigma/7) +
                        texture(snowTex, texCoord).rgb * norm_coef(h, 1.0, 5*sigma/7);


    vec3 color = diffuseColor * color_tmp;

    fragColor = vec4(color, 1.0);
}